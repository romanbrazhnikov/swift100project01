//
//  DetailViewController.swift
//  Swift100Project1
//
//  Created by Roman Brazhnikov on 09/03/2019.
//  Copyright © 2019 Roman Brazhnikov. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet var Image: UIImageView!
    var selectedImage: String?
    
    var count = 0;
    var currentNumver = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.largeTitleDisplayMode = .never
        title = "Picture \(currentNumver + 1) of \(count)"
        
        // Do any additional setup after loading the view.
        if let imageString = selectedImage {
            Image.image = UIImage(named: imageString)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.hidesBarsOnTap = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.hidesBarsOnTap = false
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
